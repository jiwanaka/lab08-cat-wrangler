#include <iostream>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>


#include "node.hpp"
#include "list.hpp"

using namespace std;

    const bool DoublyLinkedList::empty() const {
        bool status;
        if(head->next == NULL)
        status = true;
        else
        status = false;
        return status ;
    }

    void DoublyLinkedList::push_front( Node* newNode ){
        newNode->next = nullptr;
        newNode->next = head;
        head = newNode;
        nodeCount++;
        }

    Node* DoublyLinkedList::pop_front(){
        Node *tmp = head;
        head = head->next;

    return tmp;
}

    Node* DoublyLinkedList:: get_first() const {
        return head;
    }

    Node* DoublyLinkedList::get_next( const Node* currentNode ) const{
       return currentNode->next;
    }

   size_t DoublyLinkedList::size() const{
        return nodeCount;
    }
