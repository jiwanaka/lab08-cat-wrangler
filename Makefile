###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o:  cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o:  node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o:  list.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

main: main.cpp *.hpp main.o cat.o list.o node.o
	g++ -o main main.o cat.o list.o node.o

clean:
	rm -f *.o main the
