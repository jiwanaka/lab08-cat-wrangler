#pragma once

#include "node.hpp"

    class DoublyLinkedList : public Node{
        protected:
        Node* head = nullptr;
        Node* tail = nullptr;

        public:
        const bool empty() const ;
        void push_front( Node* newNode );
        Node* pop_front() ;
        Node* get_first() const;
        Node* get_next( const Node* currentNode ) const ;
        size_t size() const;

        



    };