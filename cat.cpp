///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;


void Cat::initNames() {
	if( names.empty() ) {
		cout << "Loading... ";
		
		ifstream file( CAT_NAMES_FILE );	
		string line;
		while (getline(file, line)) names.push_back(line);
		
		cout << to_string( names.size() ) << " names." << endl;
	}	
}


Cat::Cat( const std::string newName, const float newWeight, const int newDaysAlive ) {
	setName( newName );
	setWeight( newWeight );
	setDaysAlive( newDaysAlive );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );
	
	name = newName;
}


void Cat::setWeight( const float newWeight ) {
	assert( newWeight >= 0 );
	assert( newWeight <= MAX_CAT_WEIGHT );

	weight = newWeight;	
}


void Cat::setDaysAlive( const int newDaysAlive ) {
	assert( newDaysAlive >= 0 );
	assert( newDaysAlive <= MAX_CAT_AGE );
	
	daysAlive = newDaysAlive;
}


std::string Cat::getInfo() const {
	return "Cat Name = [" + name + "]"
	     + "    Weight = [" + std::to_string( weight ) + "]"
	     + "    Days Alive = [" + std::to_string( daysAlive ) + "]";
}


random_device randomDevice;        // Seed with a real random value, if available
mt19937_64 RNG( randomDevice() );  // Define a modern Random Number Generator
uniform_int_distribution<> nameRNG(0, 10000);
uniform_int_distribution<> ageRNG(0, Cat::MAX_CAT_AGE);
uniform_real_distribution<> weightRNG(0.1, Cat::MAX_CAT_WEIGHT);


Cat* Cat::makeCat() {
	return new Cat( names[nameRNG( RNG ) % names.size()], (float) weightRNG( RNG ), ageRNG( RNG ) );
}


std::vector<std::string> Cat::names;
