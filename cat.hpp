///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class Cat {
private:  /// Member variables
	std::string name;
	float       weight;
	int         daysAlive;	

private:  /// Statics
	static std::vector<std::string> names;
	
public:
	/// Constructor
	Cat( const std::string newName, const float newWeight, const int newDaysAlive );

	/// Constants
	static const int MAX_CAT_AGE = 365 * 30;
	static constexpr const float MAX_CAT_WEIGHT = 30;

	/// Setters
	void setName( const std::string newName );
	void setWeight( const float newWeight );
	void setDaysAlive( const int newDaysAlive );
	
	/// Methods
	virtual std::string getInfo() const;

	/// Static methods
	static void initNames();
	static Cat* makeCat();
};

