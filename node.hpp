#pragma once
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

    class Node{
        friend class DoublyLinkedList;

        protected:
        Node* next = nullptr;
        Node* prev = nullptr;
        size_t nodeCount = 0;

    };